package com.edu.taskand.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.edu.taskand.R;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.listener.TaskListener;
import com.edu.taskand.util.Tasks;

import java.util.ArrayList;

public class TaskListAdapter extends BaseExpandableListAdapter {

    TaskListener listener;

    public void setListener(TaskListener _listener) {
        listener = _listener;
    }

    @Override
    public int getGroupCount() {
        return Tasks.Tasks.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //Obtention du nombre des sous-tâches
        String parentId = Tasks.Tasks.get(groupPosition).TaskId;
        ArrayList<Task> t = Tasks.SubTask.get(parentId);
        int count;
        if(t != null)
            count = t.size();
        else
            count = 0;
        return count;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    //Affichage des groupes de tâches
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_task_parent, parent, false);
        ImageView btn = view.findViewById(R.id.eye_btn);
        TextView title = view.findViewById(R.id.task_parent_txt);
        ImageView status = view.findViewById(R.id.img_task_status);
        title.setText(Tasks.Tasks.get(groupPosition).Name);
        if(Tasks.Tasks.get(groupPosition).TaskStatus == 0){
             status.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp);
        } else {
            status.setImageResource(R.drawable.ic_panorama_fish_eye_yellow_24dp);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTask(Tasks.Tasks.get(groupPosition).TaskId);
            }
        });
        return view;
    }

    //Affiche des details de sous-tâche
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        String id = Tasks.Tasks.get(groupPosition).TaskId;
        if(Tasks.SubTask.get(id).get(childPosition).SubTask == 0) {
            View view = inflater.inflate(R.layout.item_task_child, parent, false);
            TextView title = view.findViewById(R.id.task_child_txt);
            ImageView status = view.findViewById(R.id.img_child);
            title.setText(Tasks.SubTask.get(id).get(childPosition).Name);
            if(Tasks.SubTask.get(id).get(childPosition).TaskStatus == 0){
                status.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp);
            } else {
                status.setImageResource(R.drawable.ic_panorama_fish_eye_yellow_24dp);
            }
            View container = view.findViewById(R.id.task_child_container);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTask(Tasks.SubTask.get(id).get(childPosition).TaskId);
                }
            });

            return view;
        } else {
            View view = inflater.inflate(R.layout.item_task_child_child, parent, false);
            TextView title = view.findViewById(R.id.task_child_txt);
            ImageView status = view.findViewById(R.id.img_child);
            title.setText(Tasks.SubTask.get(id).get(childPosition).Name);
            if(Tasks.SubTask.get(id).get(childPosition).TaskStatus == 0){
                status.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp);
            } else {
                status.setImageResource(R.drawable.ic_panorama_fish_eye_yellow_24dp);
            }
            View container = view.findViewById(R.id.task_child_container);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTask(Tasks.SubTask.get(id).get(childPosition).TaskId);
                }
            });

            return view;
        }

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
