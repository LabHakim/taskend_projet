package com.edu.taskand.util;

import android.content.Context;

import com.edu.taskand.dbprovider.SQLiteDBHelper;
import com.edu.taskand.dbprovider.item.Task;

import java.util.ArrayList;
import java.util.HashMap;

public class Tasks {
    public static ArrayList<Task> Tasks;
    public static HashMap<String, ArrayList<Task> > SubTask;

    public static void Init(Context context) {
        Tasks = new ArrayList<>();
        SubTask = new HashMap<String, ArrayList<Task>>();
        SQLiteDBHelper helper = new SQLiteDBHelper(context);
        Tasks = helper.GetParentTasks();
        // Parcours des tâches et sous-tâches
        for(int i = 0 ;i < Tasks.size(); i ++) {
            Task t = Tasks.get(i);
            String parentid = t.TaskId;
            ArrayList<Task> sub = helper.GetChildTasks(parentid);
            SubTask.put(parentid, sub);
        }
    }
}
