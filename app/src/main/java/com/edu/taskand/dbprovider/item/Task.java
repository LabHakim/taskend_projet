package com.edu.taskand.dbprovider.item;

import android.content.Context;

import com.edu.taskand.R;

public class Task {
    public int Id = -1;
    public String Name = "";
    public String Description = "";
    public int Reminder = 0;
    public String Deadline = "";
    public int Repeat = 0;
    public int Priority = 0;
    public int Difficulty = 0;
    public String ParentId = "";
    public String TaskId = "";
    public String CreateDay = "";
    public String NotifyTime = "";
    public int TaskStatus = 0;
    public String TaskNote = "";
    public String TaskFileOne = "";
    public String TaskFileTwo = "";
    public String TaskFileThree = "";
    public String SubtaskParentId = "";
    public int Completed = 0;
    public int SubTask = 0;
    public static String GetJSONStr(Task task, Context context) {

        String[] reminder = context.getResources().getStringArray(R.array.rappel_list);
        String[] repeat = context.getResources().getStringArray(R.array.repeat_list);
        String[] priority = context.getResources().getStringArray(R.array.priority_list);
        String[] difficulty = context.getResources().getStringArray(R.array.difficulty_list);

        //Generation du contenu de fichier json d une tâche
        String json = "{";
        json += GetPairedStr("name", task.Name);
        json += GetPairedStr("description", task.Description);
        json += GetPairedStr("remind", reminder[task.Reminder]);
        json += GetPairedStr("deadline", task.Deadline);
        json += GetPairedStr("repeat", repeat[task.Repeat]);
        json += GetPairedStr("priority", priority[task.Priority]);
        json += GetPairedStr("difficulty", difficulty[task.Difficulty]);
        if(task.Completed == 0) {
            json += GetPairedStr("completed", "false");
        } else {
            json += GetPairedStr("completed", "true");
        }
        json += "}";
        return json;

    }

    public static String GetPairedStr(String key, String value) {
        return key + ":" + "'" + value + "', \n";
    }
}
