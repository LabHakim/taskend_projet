package com.edu.taskand.dbprovider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.edu.taskand.common.Global;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.dbprovider.schema.TaskSchema;

import java.util.ArrayList;

public class SQLiteDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "Task.db";

    public SQLiteDBHelper(Context context) {
        super(context, DB_NAME, null, 1);
        TaskSchema.CreateTable(this.getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        TaskSchema.CreateTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Enregistrement d une tâche dans la base de donnees
    public String SaveTask(Task task) {
        String uuid = "id"+ String.valueOf(System.currentTimeMillis());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskSchema.NAME, task.Name);
        values.put(TaskSchema.DESCRIPTION, task.Description);
        values.put(TaskSchema.REMINDER, task.Reminder);
        values.put(TaskSchema.DEADLINE, task.Deadline);
        values.put(TaskSchema.REPEAT, task.Repeat);
        values.put(TaskSchema.PRIORITY, task.Priority);
        values.put(TaskSchema.DIFFICULTY, task.Difficulty);
        values.put(TaskSchema.PARENTID, task.ParentId);
        values.put(TaskSchema.TASKNOTE, task.TaskNote);
        values.put(TaskSchema.TASKSTATUS, task.TaskStatus);
        values.put(TaskSchema.FILEONE, task.TaskFileOne);
        values.put(TaskSchema.FILETWO, task.TaskFileTwo);
        values.put(TaskSchema.FILETHREE, task.TaskFileThree);
        values.put(TaskSchema.COMPLETED, task.Completed);
        values.put(TaskSchema.NOTIFITIME, task.NotifyTime);
        values.put(TaskSchema.CREATEDAY, task.CreateDay);
        values.put(TaskSchema.SUBPARENTID, task.SubtaskParentId);
        values.put(TaskSchema.TASKID, uuid);
        if(Global.isClickSubSubTask)
            values.put(TaskSchema.SUBTASK, 1);
        else
            values.put(TaskSchema.SUBTASK, 0);
        db.insert(TaskSchema.TABLE_NAME, null, values);
        db.close();
        return uuid;
    }

    //Selectionner une tache de la base de donnees
    public ArrayList<Task> GetTaskByQuery(String column, String value) {
        ArrayList<Task> tasks = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("" +
                        "SELECT * FROM " + TaskSchema.TABLE_NAME +
                        " WHERE " + column + "=" + "'" + value + "'",
                null);
        while(cursor.moveToNext()) {
            Task task = new Task();
            task.Id = cursor.getInt(cursor.getColumnIndex(TaskSchema.ID));
            task.Name = cursor.getString(cursor.getColumnIndex(TaskSchema.NAME));
            task.Description = cursor.getString(cursor.getColumnIndex(TaskSchema.DESCRIPTION));
            task.Reminder = cursor.getInt(cursor.getColumnIndex(TaskSchema.REMINDER));
            task.Deadline = cursor.getString(cursor.getColumnIndex(TaskSchema.DEADLINE));
            task.Repeat = cursor.getInt(cursor.getColumnIndex(TaskSchema.REPEAT));
            task.Priority = cursor.getInt(cursor.getColumnIndex(TaskSchema.PRIORITY));
            task.Difficulty = cursor.getInt(cursor.getColumnIndex(TaskSchema.DIFFICULTY));
            task.ParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.PARENTID));
            task.TaskId = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKID));
            task.CreateDay = cursor.getString(cursor.getColumnIndex(TaskSchema.CREATEDAY));
            task.NotifyTime = cursor.getString(cursor.getColumnIndex(TaskSchema.NOTIFITIME));
            task.Completed = cursor.getInt(cursor.getColumnIndex(TaskSchema.COMPLETED));
            task.SubTask = cursor.getInt(cursor.getColumnIndex(TaskSchema.SUBTASK));
            task.SubtaskParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.SUBPARENTID));
            tasks.add(task);
        }
        db.close();
        return tasks;
    }

    // Selectionner les tâches parentes d une tâche
    public ArrayList<Task> GetParentTasks() {
        ArrayList<Task> tasks = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("" +
                "SELECT * FROM " + TaskSchema.TABLE_NAME +
                " WHERE " + TaskSchema.PARENTID + "=0",
                null);
        while(cursor.moveToNext()) {
            Task task = new Task();
            task.Id = cursor.getInt(cursor.getColumnIndex(TaskSchema.ID));
            task.Name = cursor.getString(cursor.getColumnIndex(TaskSchema.NAME));
            task.Description = cursor.getString(cursor.getColumnIndex(TaskSchema.DESCRIPTION));
            task.Reminder = cursor.getInt(cursor.getColumnIndex(TaskSchema.REMINDER));
            task.Deadline = cursor.getString(cursor.getColumnIndex(TaskSchema.DEADLINE));
            task.Repeat = cursor.getInt(cursor.getColumnIndex(TaskSchema.REPEAT));
            task.Priority = cursor.getInt(cursor.getColumnIndex(TaskSchema.PRIORITY));
            task.Difficulty = cursor.getInt(cursor.getColumnIndex(TaskSchema.DIFFICULTY));
            task.ParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.PARENTID));
            task.TaskId = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKID));
            task.TaskStatus = cursor.getInt(cursor.getColumnIndex(TaskSchema.TASKSTATUS));
            task.TaskNote = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKNOTE));
            task.TaskFileOne = cursor.getString(cursor.getColumnIndex(TaskSchema.FILEONE));
            task.TaskFileTwo = cursor.getString(cursor.getColumnIndex(TaskSchema.FILETWO));
            task.TaskFileThree = cursor.getString(cursor.getColumnIndex(TaskSchema.FILETHREE));
            task.CreateDay = cursor.getString(cursor.getColumnIndex(TaskSchema.CREATEDAY));
            task.NotifyTime = cursor.getString(cursor.getColumnIndex(TaskSchema.NOTIFITIME));
            task.Completed = cursor.getInt(cursor.getColumnIndex(TaskSchema.COMPLETED));
            task.SubTask = cursor.getInt(cursor.getColumnIndex(TaskSchema.SUBTASK));
            task.SubtaskParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.SUBPARENTID));
            tasks.add(task);
        }
        db.close();
        return tasks;
    }

    // Selectionner les sous-tâches d une tâche
    public ArrayList<Task> GetChildTasks(String parentId) {
        ArrayList<Task> tasks = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("" +
                "SELECT * FROM " + TaskSchema.TABLE_NAME +
                " WHERE " + TaskSchema.PARENTID + "=" + "'" + parentId + "'" , null);

        while(cursor.moveToNext()) {
            Task task = new Task();
            task.Id = cursor.getInt(cursor.getColumnIndex(TaskSchema.ID));
            task.Name = cursor.getString(cursor.getColumnIndex(TaskSchema.NAME));
            task.Description = cursor.getString(cursor.getColumnIndex(TaskSchema.DESCRIPTION));
            task.Reminder = cursor.getInt(cursor.getColumnIndex(TaskSchema.REMINDER));
            task.Deadline = cursor.getString(cursor.getColumnIndex(TaskSchema.DEADLINE));
            task.Repeat = cursor.getInt(cursor.getColumnIndex(TaskSchema.REPEAT));
            task.Priority = cursor.getInt(cursor.getColumnIndex(TaskSchema.PRIORITY));
            task.Difficulty = cursor.getInt(cursor.getColumnIndex(TaskSchema.DIFFICULTY));
            task.ParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.PARENTID));
            task.TaskId = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKID));
            task.TaskStatus = cursor.getInt(cursor.getColumnIndex(TaskSchema.TASKSTATUS));
            task.TaskNote = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKNOTE));
            task.TaskFileOne = cursor.getString(cursor.getColumnIndex(TaskSchema.FILEONE));
            task.TaskFileTwo = cursor.getString(cursor.getColumnIndex(TaskSchema.FILETWO));
            task.TaskFileThree = cursor.getString(cursor.getColumnIndex(TaskSchema.FILETHREE));
            task.CreateDay = cursor.getString(cursor.getColumnIndex(TaskSchema.CREATEDAY));
            task.NotifyTime = cursor.getString(cursor.getColumnIndex(TaskSchema.NOTIFITIME));
            task.Completed = cursor.getInt(cursor.getColumnIndex(TaskSchema.COMPLETED));
            task.SubTask = cursor.getInt(cursor.getColumnIndex(TaskSchema.SUBTASK));
            task.SubtaskParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.SUBPARENTID));
            tasks.add(task);
        }
        db.close();
        return tasks;
    }

    // Selectionner une tache de la base de donnnes a partir de son ID
    public Task GetTask(String taskId) {
        Task task = new Task();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("" +
                "SELECT * FROM " + TaskSchema.TABLE_NAME +
                " WHERE " + TaskSchema.TASKID + "=" + "'" + taskId + "'" , null);
        while(cursor.moveToNext()) {
            task.Id = cursor.getInt(cursor.getColumnIndex(TaskSchema.ID));
            task.Name = cursor.getString(cursor.getColumnIndex(TaskSchema.NAME));
            task.Description = cursor.getString(cursor.getColumnIndex(TaskSchema.DESCRIPTION));
            task.Reminder = cursor.getInt(cursor.getColumnIndex(TaskSchema.REMINDER));
            task.Deadline = cursor.getString(cursor.getColumnIndex(TaskSchema.DEADLINE));
            task.Repeat = cursor.getInt(cursor.getColumnIndex(TaskSchema.REPEAT));
            task.Priority = cursor.getInt(cursor.getColumnIndex(TaskSchema.PRIORITY));
            task.Difficulty = cursor.getInt(cursor.getColumnIndex(TaskSchema.DIFFICULTY));
            task.ParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.PARENTID));
            task.TaskId = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKID));
            task.TaskStatus = cursor.getInt(cursor.getColumnIndex(TaskSchema.TASKSTATUS));
            task.TaskNote = cursor.getString(cursor.getColumnIndex(TaskSchema.TASKNOTE));
            task.TaskFileOne = cursor.getString(cursor.getColumnIndex(TaskSchema.FILEONE));
            task.TaskFileTwo = cursor.getString(cursor.getColumnIndex(TaskSchema.FILETWO));
            task.TaskFileThree = cursor.getString(cursor.getColumnIndex(TaskSchema.FILETHREE));
            task.CreateDay = cursor.getString(cursor.getColumnIndex(TaskSchema.CREATEDAY));
            task.NotifyTime = cursor.getString(cursor.getColumnIndex(TaskSchema.NOTIFITIME));
            task.Completed = cursor.getInt(cursor.getColumnIndex(TaskSchema.COMPLETED));
            task.SubTask = cursor.getInt(cursor.getColumnIndex(TaskSchema.SUBTASK));
            task.SubtaskParentId = cursor.getString(cursor.getColumnIndex(TaskSchema.SUBPARENTID));
        }
        db.close();
        return task;
    }

    //Mettre a jour une tache dans la base de donnees
    public void UpdateTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskSchema.NAME, task.Name);
        values.put(TaskSchema.DESCRIPTION, task.Description);
        values.put(TaskSchema.REMINDER, task.Reminder);
        values.put(TaskSchema.DEADLINE, task.Deadline);
        values.put(TaskSchema.REPEAT, task.Repeat);
        values.put(TaskSchema.PRIORITY, task.Priority);
        values.put(TaskSchema.DIFFICULTY, task.Difficulty);
        values.put(TaskSchema.PARENTID, task.ParentId);
        values.put(TaskSchema.TASKSTATUS, task.TaskStatus);
        values.put(TaskSchema.TASKNOTE, task.TaskNote);
        values.put(TaskSchema.FILEONE, task.TaskFileOne);
        values.put(TaskSchema.FILETWO, task.TaskFileTwo);
        values.put(TaskSchema.FILETHREE, task.TaskFileThree);
        values.put(TaskSchema.NOTIFITIME, task.NotifyTime);
        values.put(TaskSchema.CREATEDAY, task.CreateDay);
        values.put(TaskSchema.COMPLETED, task.Completed);
        values.put(TaskSchema.SUBTASK, task.SubTask);
        values.put(TaskSchema.SUBPARENTID, task.SubtaskParentId);
        db.update(TaskSchema.TABLE_NAME, values, TaskSchema.TASKID + "=" + "'" + task.TaskId + "'", null);
        db.close();
    }

    //Mettre a jour le statut d une tache dans la base de donnees
    public void UpdateTaskStatus(String taskId, int status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskSchema.TASKSTATUS, status);
        db.update(TaskSchema.TABLE_NAME, values, TaskSchema.TASKID + "=" + "'" + taskId + "'", null);
        db.close();
    }

    //Supprimer une tache de la base de donnnes
    public void DeleteTask(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TaskSchema.TABLE_NAME + " where " + TaskSchema.TASKID + " = '" + id + "'" );
        db.close();
    }
}
