package com.edu.taskand.dbprovider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.edu.taskand.service.TaskService;

public class TaskReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent _intent = new Intent(context, TaskService.class);
        context.startService(_intent);
    }
}
