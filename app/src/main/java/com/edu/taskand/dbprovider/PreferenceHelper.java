package com.edu.taskand.dbprovider;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {
    private static SharedPreferences pref;
    public static void Init(Context context) {
        pref = context.getSharedPreferences("data", Context.MODE_PRIVATE);
    }

    public static void setAttach(String taskId, String uri) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(taskId, uri);
        editor.commit();
    }

    public static String getAttach(String taskId) {
        String uri = pref.getString(taskId, "");
        return uri;
    }
}
