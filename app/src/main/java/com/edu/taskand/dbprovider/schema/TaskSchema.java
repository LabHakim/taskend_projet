package com.edu.taskand.dbprovider.schema;

import android.database.sqlite.SQLiteDatabase;

public class TaskSchema  {
    public static String TABLE_NAME = "TaskTable";
    public static String ID = "id";
    public static String NAME = "name";
    public static String DESCRIPTION = "description";
    public static String REMINDER = "reminder";
    public static String DEADLINE = "deadline";
    public static String REPEAT = "repeat";
    public static String PRIORITY = "priority";
    public static String DIFFICULTY = "difficulty";
    public static String PARENTID = "parentId";
    public static String TASKID = "taskId";
    public static String TASKSTATUS = "status";
    public static String TASKNOTE = "note";
    public static String FILEONE = "file_one";
    public static String FILETWO = "file_two";
    public static String FILETHREE = "file_three";
    public static String CREATEDAY = "createdDay";
    public static String NOTIFITIME = "notifyTime";
    public static String COMPLETED = "completed";
    public static String SUBTASK = "subtask";
    public static String SUBPARENTID = "subParentId";


    //Creation de tables de base de donnees
    public static void CreateTable(SQLiteDatabase db) {
        db.execSQL(
                  "create table if not exists "
                + TABLE_NAME
                + " ( "
                          + ID + " integer primary key AUTOINCREMENT, "
                          + NAME + " varchar, "
                          + DESCRIPTION + " text, "
                          + REMINDER + " integer, "
                          + DEADLINE + " varchar, "
                          + REPEAT + " integer, "
                          + PRIORITY + " integer, "
                          + DIFFICULTY + " integer, "
                          + PARENTID + " varchar, "
                          + TASKID + " varchar, "
                          + TASKSTATUS + " integer, "
                          + TASKNOTE + " varchar, "
                          + FILEONE + " varchar, "
                          + FILETWO + " varchar, "
                          + FILETHREE + " varchar, "
                          + CREATEDAY + " varchar,"
                          + NOTIFITIME + " varchar, "
                          + COMPLETED + " integer, "
                          + SUBTASK + " integer, "
                          + SUBPARENTID + " varchar "
                + " ) "
        );

    }
}
