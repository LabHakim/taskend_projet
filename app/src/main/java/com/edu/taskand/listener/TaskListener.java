package com.edu.taskand.listener;

public interface TaskListener {
    abstract public void onTask(String taskId);
}
