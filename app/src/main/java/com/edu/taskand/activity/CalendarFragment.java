package com.edu.taskand.activity;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edu.taskand.R;
import com.edu.taskand.views.CalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
public class CalendarFragment extends Fragment {

    CalendarView calendarView;
    //Page de calendrier
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.activity_calendar, container, false);
        calendarView = layout.findViewById(R.id.calendarView);
        return layout;
    }
}
