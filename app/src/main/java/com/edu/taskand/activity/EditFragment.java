package com.edu.taskand.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.telephony.gsm.GsmCellLocation;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.edu.taskand.BuildConfig;
import com.edu.taskand.R;
import com.edu.taskand.adapter.OptionSpinnerAdapter;
import com.edu.taskand.common.Global;
import com.edu.taskand.dbprovider.SQLiteDBHelper;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.listener.BackListener;
import com.edu.taskand.util.Tasks;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.nio.file.FileVisitOption;
import java.util.ArrayList;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;

public class EditFragment extends Fragment {
    LinearLayout subList;
    ImageButton addSubButton;
    Spinner spinner1, spinner2, spinner3, spinner4;
    OptionSpinnerAdapter reminderAdapter, repeatAdapter , priorityAdapter, difficultyAdapter;
    ImageButton menubtn;
    Button deadline;
    BackListener backListener;
    EditText name;
    EditText description;
    LinearLayout layNote;
    LinearLayout layFiles;
    TextView txtNote;
    TextView txtFirstFile;
    TextView txtSecondFile;
    TextView txtThirdFile;
    ImageView imgFirstFile;
    ImageView imgSecondFile;
    ImageView imgThirdFile;

    String firstFile = "";
    String secondFile = "";
    String thirdFile = "";
    String note = "";
    String deadlineStr = "";
    ArrayList<EditText> subTasks;
    Task curTask = null;
    boolean isParent = true;
    ArrayList<Task> subTaskList = new ArrayList<>();
    View subTaskForm;
    int isClickIndex = 0;
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.activity_edit, container, false);
        thisView = layout;
        subList = layout.findViewById(R.id.add_subtask_list);
        addSubButton = layout.findViewById(R.id.add_subtask_btn);
        spinner1 = layout.findViewById(R.id.spinner1);
        spinner2 = layout.findViewById(R.id.spinner2);
        spinner3 = layout.findViewById(R.id.spinner3);
        spinner4 = layout.findViewById(R.id.spinner4);
        menubtn = layout.findViewById(R.id.menu_btn);
        deadline = layout.findViewById(R.id.edit_deadline_btn);
        name = layout.findViewById(R.id.edit_name);
        description = layout.findViewById(R.id.edit_description);
        subTaskForm = layout.findViewById(R.id.edit_subtask_form);
        layNote = layout.findViewById(R.id.lay_note);
        txtNote = layout.findViewById(R.id.txt_note);
        layFiles = layout.findViewById(R.id.lay_files);
        txtFirstFile = layout.findViewById(R.id.txt_first_file);
        txtSecondFile = layout.findViewById(R.id.txt_second_file);
        txtThirdFile = layout.findViewById(R.id.txt_third_file);
        imgFirstFile = layout.findViewById(R.id.img_first_file);
        imgSecondFile = layout.findViewById(R.id.img_second_file);
        imgThirdFile = layout.findViewById(R.id.img_third_file);

        ArrayList<Task> subTaskList;
        deadline.setOnClickListener(new View.OnClickListener(){

            //Mettre en place d un picker pour selection de date
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String dateStr = "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                deadline.setText(dateStr);
                                deadlineStr = dateStr;
                            }
                        }, 2020, 4, 16);
                datePickerDialog.show();
            }
        });
        Global.taskStatus = 0;
        Global.isDeleteClick = false;
        menubtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(EditFragment.this.getActivity(), v);

                popup.getMenuInflater()
                        .inflate(R.menu.pop_menu, popup.getMenu());

                // Menu d une tâche cree ou d une nouvelle tache
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getTitle().equals("Joindre un fichier")) {
                            // Joindre un fichier a la tache actuelle
                            showFileChooser();
                        } else if(item.getTitle().equals("Ajouter une note")){
                            // Ajouter une note a la tache actuelle
                            isClickIndex = 2;
                            showNoteDialog();
                        } else if(item.getTitle().equals("Marquer comme terminé")) {
                            // Marquer la tâche actuelle comme terminé
                            isClickIndex = 3;
                            if(curTask != null) {
                                SQLiteDBHelper helper = new SQLiteDBHelper(getActivity());
                                helper.UpdateTaskStatus(curTask.TaskId, 1);
                            }
                            Global.taskStatus = 1;
                        } else if(item.getTitle().equals("Supprimer la tâche")) {
                            // Supprimer la tache actuelle
                            isClickIndex = 4;
                            showDeleteDialog();
                        } else if(item.getTitle().equals("Partager la tâche")) {
                            // Generation d un fichier Json et envoie
                            isClickIndex = 5;
                            if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
                            } else {
                                shareTask();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });


        // Gestion des proporitete d une tâche
        reminderAdapter = new OptionSpinnerAdapter();
        repeatAdapter = new OptionSpinnerAdapter();
        priorityAdapter = new OptionSpinnerAdapter();
        difficultyAdapter = new OptionSpinnerAdapter();

        reminderAdapter.setData(getActivity().getResources().getStringArray(R.array.rappel_list));
        repeatAdapter.setData(getActivity().getResources().getStringArray(R.array.repeat_list));
        priorityAdapter.setData(getActivity().getResources().getStringArray(R.array.priority_list));
        difficultyAdapter.setData(getActivity().getResources().getStringArray(R.array.difficulty_list));

        spinner1.setAdapter(reminderAdapter);
        spinner2.setAdapter(repeatAdapter);
        spinner3.setAdapter(priorityAdapter);
        spinner4.setAdapter(difficultyAdapter);

        addSubButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddSubTask(new Task());
            }
        });

        subTasks = new ArrayList<>();

        // Definition des differentes elements de proprietes d une têche
        if(curTask != null) {
            if(!curTask.ParentId.equals("0")) {
                Global.isClickSubSubTask = true;
                if(curTask.SubTask == 0){
                    Global.isClickSubTask = true;
                }
            }
            name.setText( curTask.Name);
            description.setText(curTask.Description);
            if(!curTask.TaskNote.equals(""))
            {
                layNote.setVisibility(View.VISIBLE);
                txtNote.setText(curTask.TaskNote);

            }
            if(!curTask.TaskFileOne.equals("")){
                layFiles.setVisibility(View.VISIBLE);
                txtFirstFile.setText(curTask.TaskFileOne);
                txtSecondFile.setText(curTask.TaskFileTwo);
                txtThirdFile.setText(curTask.TaskFileThree);
            }
            spinner1.setSelection(curTask.Reminder);
            spinner2.setSelection(curTask.Repeat);
            spinner3.setSelection(curTask.Priority);
            spinner4.setSelection(curTask.Difficulty);
            if(curTask.Deadline.compareTo("") != 0) {
                deadline.setText(curTask.Deadline);
            }
            note = curTask.TaskNote;
            firstFile = curTask.TaskFileOne;
            secondFile = curTask.TaskFileTwo;
            thirdFile = curTask.TaskFileThree;

            if(curTask.SubTask == 0) {
                ArrayList<Task> subs = Tasks.SubTask.get(curTask.TaskId);
                if (subs != null) {
                    for (int i = 0; i < subs.size(); i++) {
                        onAddSubTask(subs.get(i));
                    }
                }
                subTaskForm.setVisibility(View.VISIBLE);
            } else {
                subTaskForm.setVisibility(View.GONE);
            }
        }

        return layout;
    }


    // fonction pour ajout d une sous-tâche au tâche actuelle
    private void onAddSubTask(Task task) {
        LayoutInflater inflater = LayoutInflater.from(this.getActivity());
        View sub = inflater.inflate(R.layout.item_sub_task, subList, false);
        EditText edit = sub.findViewById(R.id.item_sub_task_txt);
        edit.setText(task.Name);
        subList.addView(sub);

        subTasks.add(edit);
        subTaskList.add(task);
    }

    public void setBackListener(BackListener _listener) {
        backListener = _listener;
    }

    //Gestion de la tâche
    public Task getMainTask() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int dayofweek = calendar.get(Calendar.DAY_OF_WEEK);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        String created = "" + day + ":" +dayofweek;

        int remindType = spinner1.getSelectedItemPosition();
        String notifyTime = "";
        switch (remindType) {
            case 0:
                notifyTime = "" + (hour + 1) + ":" + min;
                break;
            case 1:
                notifyTime = "12:0";
                break;
            case 2:
                notifyTime = "18:0";
                break;
            case 3:
                notifyTime = "9:0";
                break;
        }

        //Affectation des propriétés de la tâche a une tâche
        Task task = new Task();
        task.Name = name.getText().toString();
        task.Description = description.getText().toString();
        task.TaskNote = note;
        task.TaskStatus = Global.taskStatus;
        task.TaskFileOne = firstFile;
        task.TaskFileTwo = secondFile;
        task.TaskFileThree = thirdFile;
        task.Deadline = deadlineStr;
        task.Reminder = spinner1.getSelectedItemPosition();
        task.Repeat = spinner2.getSelectedItemPosition();
        task.Priority = spinner3.getSelectedItemPosition();
        task.Difficulty = spinner4.getSelectedItemPosition();
        task.CreateDay = created;
        task.NotifyTime = notifyTime;
        if(Global.isClickSubSubTask)
        {
            task.SubTask = 1;
            task.SubtaskParentId = curTask.SubtaskParentId;
        }

        if(Global.isClickSubTask) {
            task.SubTask = 0;
        }
        if(isParent) {
            task.ParentId = "0";
        } else {
            task.ParentId = curTask.ParentId;
        }
        return task;
    }

    // Obtention de la liste des sous-tâches des tâches
    public ArrayList<Task> getChildTasks(String id) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int dayofweek = calendar.get(Calendar.DAY_OF_WEEK);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        String created = "" + day + ":" +dayofweek;
        for(int i = 0 ;i  < subTaskList.size() ; i ++) {
            subTaskList.get(i).ParentId = id;
            subTaskList.get(i).Name = subTasks.get(i).getText().toString();
            subTaskList.get(i).CreateDay = created;
        }
        return this.subTaskList;
    }
    private static final int FILE_SELECT_CODE = 0;


    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Selectionner un fichier à importer"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Redirectionner l utilisateur au Store pour telecharger un gestionnnaire de fichiers
            Toast.makeText(getActivity(), "Merci d'installer un gestionnaire de fichiers.",
                    Toast.LENGTH_SHORT).show();
        }
    }



    //Gestion des fichiers joints
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Obtenir le uri du fichier
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;

                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (uriString.startsWith("file://")) {
                        displayName = myFile.getName();
                    }

                    if(firstFile.equals("")) {
                        firstFile = displayName;
                        if(myFile.exists()){
                            Bitmap myBitmap = BitmapFactory.decodeFile(myFile.getAbsolutePath());
                            imgFirstFile.setImageBitmap(myBitmap);

                        }
                    } else if(secondFile.equals("")) {
                        secondFile = displayName;
                        if(myFile.exists()){
                            Bitmap myBitmap = BitmapFactory.decodeFile(myFile.getAbsolutePath());
                            imgSecondFile.setImageBitmap(myBitmap);

                        }
                    } else if(thirdFile.equals("")) {
                        thirdFile = displayName;
                        if(myFile.exists()){
                            Bitmap myBitmap = BitmapFactory.decodeFile(myFile.getAbsolutePath());
                            imgThirdFile.setImageBitmap(myBitmap);

                        }
                    }
                    layFiles.setVisibility(View.VISIBLE);
                    txtFirstFile.setText(firstFile);
                    txtSecondFile.setText(secondFile);
                    txtThirdFile.setText(thirdFile);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setTask(Task task) {
        curTask = task;
        if (task.ParentId.compareTo("0") == 0)
            isParent = true;
        else
            isParent = false;
    }

    public boolean isParentTask() {
        return isParent;
    }


    // Generer un fichier json et le partager
    public void shareTask(){
        if(curTask != null) {
            JSONObject task = new JSONObject();
            try {
                task.put("TaskId", curTask.Id);
                task.put("Name", curTask.Name);
                task.put("Description", curTask.Description);
                task.put("Reminder", curTask.Reminder);
                task.put("Priority", curTask.Priority);
                task.put("Deadline", curTask.Deadline);
                task.put("Repeat", curTask.Repeat);
                task.put("Difficulty", curTask.Difficulty);
                if(!txtNote.getText().toString().equals(""))
                    task.put("Note", txtNote.getText().toString());
                if(!txtFirstFile.getText().toString().equals(""))
                    task.put("Attached File", txtFirstFile.getText().toString());
                if(!txtSecondFile.getText().toString().equals(""))
                    task.put("Attached File", txtSecondFile.getText().toString());
                if(!txtThirdFile.getText().toString().equals(""))
                    task.put("Attached File", txtThirdFile.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String jsonName = "Task.json";
            String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Taskand";
            File dir = new File(dirPath);
            if (!dir.mkdir()) {
                dir.mkdir();
            }
            if(!dir.exists()){
                dir.mkdir();
            }
            File file = new File(dirPath, jsonName);
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(task.toString());
                myOutWriter.close();
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Uri uri = FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                uri.normalizeScheme();
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("*/*");

            intent.putExtra(Intent.EXTRA_SUBJECT, "");
            intent.putExtra(Intent.EXTRA_TEXT, "");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            try {
                startActivity(Intent.createChooser(intent, "Task.json"));
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getActivity(), "No App Available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("*/*");
                    if (intent.resolveActivity(getActivity().getApplicationContext().getPackageManager()) != null) {
                        shareTask();
                    } else {
                        Toast.makeText(getActivity(), "Permission error", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    // Dialogues
    public Activity thisActivity;
    public Context thisContext;
    public View thisView;

    public PopupWindow popupWindow;
    public int mWidth;
    public int mHeight;
    public void calWidthAndHeight(Context context) {
        WindowManager wm= (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics= new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        mWidth = metrics.widthPixels;
        mHeight = metrics.heightPixels;
    }

    public void changeWindowAlfa(float alfa) {
        WindowManager.LayoutParams params = getActivity().getWindow().getAttributes();
        params.alpha = alfa;
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getActivity().getWindow().setAttributes(params);
    }

    //Affichage du champ d'ajout de note
    public void showNoteDialog() {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View vPopupWindow = inflater.inflate(R.layout.dialog_note, null, false);
            popupWindow = new PopupWindow(vPopupWindow, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);


            calWidthAndHeight(getActivity());

            popupWindow.setWidth(mWidth);
            popupWindow.setHeight(mHeight);
            popupWindow.setOutsideTouchable(false);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    changeWindowAlfa(1f);//pop消失，透明度恢复
                }
            });
            popupWindow.showAtLocation(thisView, Gravity.CENTER|Gravity.TOP, 0, 0);

            changeWindowAlfa(0.5f);

            EditText edtContent = (EditText)vPopupWindow.findViewById(R.id.edt_note);
            TextView btnOK = (TextView) vPopupWindow.findViewById(R.id.dialog_ok);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    note = edtContent.getText().toString();
                    clickConfirmButton();
                }
            });

            TextView btnCancel = (TextView) vPopupWindow.findViewById(R.id.dialog_cancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickCancelButton();
                }
            });
        } catch (Exception e) {

        }
    }


    //Affichage du dialogue de suppression de tâche
    @TargetApi(Build.VERSION_CODES.M)
    public void showDeleteDialog() {
        try {
            LayoutInflater inflater = (LayoutInflater)  getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View vPopupWindow = inflater.inflate(R.layout.dialog_delete, null, false);
            popupWindow = new PopupWindow(vPopupWindow, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);

            calWidthAndHeight(getActivity());

            popupWindow.setWidth(mWidth);
            popupWindow.setHeight(mHeight);
            popupWindow.setOutsideTouchable(false);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    changeWindowAlfa(1f);
                }
            });
            popupWindow.showAtLocation(thisView, Gravity.CENTER|Gravity.TOP, 0, 0);

            changeWindowAlfa(0.5f);

            TextView btnOK = (TextView) vPopupWindow.findViewById(R.id.dialog_ok);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickConfirmButton();
                }
            });

            TextView btnCancel = (TextView) vPopupWindow.findViewById(R.id.dialog_cancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickCancelButton();
                }
            });
        } catch (Exception e) {

        }
    }
    public void closeDialog(){
        if (popupWindow != null) {
            popupWindow.dismiss();
            popupWindow = null;
        }
    }

    //Ajouter une note ou supprimer la tache actuelle
    public void clickConfirmButton() {
        closeDialog();
        if(isClickIndex == 2) {
            layNote.setVisibility(View.VISIBLE);
            txtNote.setText(note);
        }

        if(isClickIndex == 4) {
            if(curTask!= null){
                Global.isDeleteClick = true;
                SQLiteDBHelper helper = new SQLiteDBHelper(getActivity());
                helper.DeleteTask(curTask.TaskId);
                getActivity().onBackPressed();
            }
        }
    }

    //Cliquer sur OK pour fermer
    public void clickOkButton(){
        closeDialog();
    }
    // Cliquer sur Cancel pour Annuler
    public void clickCancelButton(){
        closeDialog();
    }


}
