package com.edu.taskand.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.edu.taskand.R;
import com.edu.taskand.adapter.OptionSpinnerAdapter;
import com.edu.taskand.adapter.TaskListAdapter;
import com.edu.taskand.common.Global;
import com.edu.taskand.dbprovider.SQLiteDBHelper;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.util.Constant;

import java.util.ArrayList;
import java.util.Calendar;

public class ViewActivity extends BaseActivity {
    String taskId;
    EditFragment fragment;
    Task task;
    SQLiteDBHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        //Affichage de la tâche selectionnee avec possibilite d edition
        thisActivity = this;
        helper = new SQLiteDBHelper(this);
        taskId = getIntent().getStringExtra("TASKID");
        task = helper.GetTask(taskId);

        fragment = new EditFragment();

        getFragmentManager().beginTransaction().replace(R.id.view_content , fragment).commit();
        fragment.setTask(task);

        TextView dateTime;
        dateTime = findViewById(R.id.top_bar_time);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        String datestr = "" + day + "." + month + ", " + Constant.weeks[week-1];
        dateTime.setText(datestr);
    }

    // Enrergistrer, annuler ou ignorer les modifications apportees a la tâche au clic du bouton arrière du telephone
    @Override
    public void onBackPressed() {
        if(!Global.isDeleteClick) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Enregistrer?")
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onback(true);
                        }
                    })
                    .setNegativeButton("Ignorer", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onback(false);
                        }
                    })
                    .setNeutralButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.create().show();
        } else{
            super.onBackPressed();
            Global.isDeleteClick = false;
        }
    }

    public void onback(boolean save) {
        if(save) {
            SQLiteDBHelper helper = new SQLiteDBHelper(this);
            Task mainTask = fragment.getMainTask();
            mainTask.TaskId = this.taskId;

            helper.UpdateTask(mainTask);
            ArrayList<Task> subTasks = fragment.getChildTasks(this.taskId);

            for(int i = 0 ; i < subTasks.size() ; i ++) {
                Task sub = subTasks.get(i);
                if (sub.Id == -1) {
                    if(Global.isClickSubTask) {
                        sub.SubTask = 1;
                        sub.SubtaskParentId = sub.ParentId;
                        sub.ParentId = mainTask.ParentId;
                    }
                    else
                        sub.SubTask = 0;
                    helper.SaveTask(sub);
                } else {
                    helper.UpdateTask(sub);
                }
            }
            Global.isClickSubTask = false;
            Global.isClickSubSubTask = false;
        } else {

        }
        super.onBackPressed();
    }
}
