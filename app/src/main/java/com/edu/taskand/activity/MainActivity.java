package com.edu.taskand.activity;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.edu.taskand.R;
import com.edu.taskand.util.Constant;

import java.util.Calendar;

public class MainActivity extends Activity {

    Fragment curFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Mettre en place du calendrier sur le header sous forme 'Day.Month Day of week'
        TextView dateTime;
        dateTime = findViewById(R.id.top_bar_time);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        String datestr = "" + day + "." + month + ", " + Constant.weeks[week-1];
        dateTime.setText(datestr);

        onHome(null);
    }

    //Appel a l ecran de calendrier pour remplacer l ecran d acceuil
    public void onCalendar(View v) {
        curFragment = new CalendarFragment();
        getFragmentManager().beginTransaction().replace(R.id.main_fragment, curFragment).commit();
    }


    //Appel pour retour a l ecran de l acceuil
    public void onHome(View v) {
        curFragment = new HomeActiviy();
        getFragmentManager().beginTransaction().replace(R.id.main_fragment, curFragment).commit();
    }


    //Appel a l ecran d edition  pour remplacer l ecran d acceuil
    public void onEdit(View v) {
        curFragment = new EditFragment();
        getFragmentManager().beginTransaction().replace(R.id.main_fragment, curFragment).commit();
    }

}
