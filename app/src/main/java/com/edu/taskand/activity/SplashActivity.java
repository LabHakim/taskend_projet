package com.edu.taskand.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.SyncStateContract;

import com.edu.taskand.R;
import com.edu.taskand.dbprovider.PreferenceHelper;
import com.edu.taskand.dbprovider.SQLiteDBHelper;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.service.TaskService;
import com.edu.taskand.util.Tasks;

import java.util.ArrayList;
import java.util.Calendar;

public class SplashActivity extends Activity implements ServiceConnection {

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Liaison avec ecran activity_splash
        setContentView(R.layout.activity_splash);

        Tasks.Init(this);
        PreferenceHelper.Init(this);

        //Temps en milisecondes de la duree du splash screen avant de disparaitre
        handler.sendEmptyMessageDelayed(0, 3000);

        Intent intent = new Intent(this, TaskService.class);
        startService(intent);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage( Message msg) {
            super.handleMessage(msg);
            //Redirection vers la page d acceuil de l app - MainActivity
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }
}
