package com.edu.taskand.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import com.edu.taskand.R;
import com.edu.taskand.adapter.TaskListAdapter;
import com.edu.taskand.listener.TaskListener;
import com.edu.taskand.util.Tasks;

public class HomeActiviy extends Fragment implements TaskListener {

    ExpandableListView m_taskList;
    TaskListAdapter mAdapter;
    ImageButton addButton;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.activity_home, container, false);

        // Appel aux vues de la liste des taches et bouton ajouter
        m_taskList = layout.findViewById(R.id.main_task_list);
        addButton = layout.findViewById(R.id.main_add_button);

        mAdapter = new TaskListAdapter();
        mAdapter.setListener(this);

        m_taskList.setAdapter(mAdapter);

        //Appel du listenner sur le bouton d ajout d une tâche
        addButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onAddTask();
            }
        });

        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        Tasks.Init(this.getActivity());
        mAdapter.notifyDataSetChanged();
    }

    private void onAddTask() {
        //Redirection vers page d ajout de tâches
        Intent intent = new Intent(this.getActivity(), AddActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTask(String id) {
        // Redirection vers page d affichage de tâche
        Intent intent = new Intent(this.getActivity(), ViewActivity.class);
        intent.putExtra("TASKID", id);
        startActivity(intent);
    }
}
