package com.edu.taskand.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.edu.taskand.R;
import com.edu.taskand.adapter.OptionSpinnerAdapter;
import com.edu.taskand.common.Global;
import com.edu.taskand.dbprovider.SQLiteDBHelper;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.util.Constant;

import java.util.ArrayList;
import java.util.Calendar;

public class AddActivity extends Activity {

    EditFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        TextView dateTime;
        dateTime = findViewById(R.id.top_bar_time);

        //Affichage de date en header
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        String datestr = "" + day + "." + month + ", " + Constant.weeks[week-1];
        dateTime.setText(datestr);

        //Appel au Fragment d edtion d une tâche

        fragment = new EditFragment();
        getFragmentManager().beginTransaction().replace(R.id.add_content, fragment).commit();
    }


    //
    @Override
    public void onBackPressed() {
        //Enregistrer, annuler, ignorer apres avoir cliqué sur le bouton arrière du telephone
        AlertDialog.Builder builder  =  new AlertDialog.Builder(this)
                .setTitle("Enregistrer ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onback(true);
                    }
                })
                .setNegativeButton("Ignorer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onback(false);
                    }
                })
                .setNeutralButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }
    public void onback(boolean save) {
        if(save) {
            SQLiteDBHelper helper = new SQLiteDBHelper(this);
            Task task = fragment.getMainTask();
            String id = helper.SaveTask(task);
            ArrayList<Task> subTasks = fragment.getChildTasks(id);

            for(int i = 0 ; i < subTasks.size() ; i ++) {
                Task sub = subTasks.get(i);
                sub.ParentId = id;
                if(Global.isClickSubSubTask) {
                    sub.ParentId = task.ParentId;
                    sub.SubtaskParentId = id;
                }
                helper.SaveTask(sub);
            }

        } else {

        }
        super.onBackPressed();
    }
}
