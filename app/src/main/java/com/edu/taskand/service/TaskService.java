package com.edu.taskand.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.edu.taskand.R;
import com.edu.taskand.activity.SplashActivity;
import com.edu.taskand.dbprovider.SQLiteDBHelper;
import com.edu.taskand.dbprovider.TaskReceiver;
import com.edu.taskand.dbprovider.item.Task;
import com.edu.taskand.dbprovider.schema.TaskSchema;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TaskService extends Service {

    public static String SERVICE_CONNECTOR = "com.edu.service.connector";

    private final int UPDATE_INTERVAL = 60 * 1000;
    private Timer timer = new Timer();
    private static final int NOTIFICATION_EX = 1;
    private NotificationManager notificationManager;


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("service", "destroy");
        Intent intent = new Intent(getBaseContext(), TaskReceiver.class);
        sendBroadcast(intent);
    }

    //Handler pour gerer les diffrentes notifications d echeance et repetition de tâche
    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("service", "run");



            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);
            String time = "" + hour + ":" + min;

            SQLiteDBHelper helper = new SQLiteDBHelper(getBaseContext());
            ArrayList<Task> tasks = helper.GetTaskByQuery(TaskSchema.NOTIFITIME, time);
            ArrayList<Task> availableTask = new ArrayList<>();

            for(int i = 0; i < tasks.size() ; i ++) {
                String deadline = tasks.get(i).Deadline;
                if (deadline.compareTo("") != 0) {
                    String[] d = deadline.split("/");
                    Date date = new Date();
                    date.setYear(Integer.valueOf(d[2]));
                    date.setMonth(Integer.valueOf(d[1]));
                    date.setDate(Integer.valueOf(d[0]));

                    if (calendar.getTime().after(date) ){
                        continue;
                    }
                }

                int repeater = tasks.get(i).Repeat;
                if (repeater == 0) {

                } else if(repeater == 1) {
                    int week = Integer.valueOf(tasks.get(i).CreateDay.split(":")[1]);
                    if (week != calendar.get(Calendar.DAY_OF_WEEK)) {
                        continue;
                    }
                } else {
                    int day = Integer.valueOf(tasks.get(i).CreateDay.split(":")[0]);
                    if (day != calendar.get(Calendar.DAY_OF_MONTH)) {
                        continue;
                    }
                }

                availableTask.add(tasks.get(i));

            }

            //Mettre en place les notifications programmées
            if(availableTask.size() > 0) {

                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);

                Notification n = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Taskend notification")
                        .setContentText("C'est le temps de : " + tasks.size())
                        .setSmallIcon(R.drawable.ic_add)
                        .setContentIntent(pIntent)
                        .setAutoCancel(true)
                        .build();


                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, n);
            }
        }
    };

    @Override
    public int onStartCommand(Intent _intent, int flags, int startid) {

        IntentFilter filter = new IntentFilter();
        filter.addAction(SERVICE_CONNECTOR);
        ServiceBroadCast receiver = new ServiceBroadCast();
        registerReceiver(receiver, filter);
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                // Check if there are updates here and notify if true
                handler.sendEmptyMessage(0);
            }
        }, 0, UPDATE_INTERVAL);
        return START_STICKY;
    }

    private void stopService() {
        if (timer != null) timer.cancel();
    }

    class ServiceBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent _intent = new Intent(context, TaskService.class);
            context.startService(_intent);
        }
    }
}
